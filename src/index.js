import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import axios from 'axios';

import * as serviceWorker from './serviceWorker';

import App from './components/App';
import configureStore from './store/configureStore';
import { initUser } from './actions/userActions';

import './css/bootstrap.min.css';
import './css/style.css';

let dynUrl;

if(window.location.hostname =='localhost'){
	dynUrl = 'http://localhost/devreviewgrowthapi/api';
} else {
   dynUrl = 'https://reviewgrowth.com/reviewgrowthapi/api/';
}

export const scAxios = axios.create({
    baseURL: dynUrl,
});

export const thirdparty = axios.create({
    baseURL: 'http://',
});


const store = configureStore();
store.dispatch(initUser());

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();

import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';

import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE } from '../../constants';

import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import {CardElement, injectStripe, Elements, StripeProvider, CardNumberElement, CardExpiryElement, CardCVCElement, PostalCodeElement } from 'react-stripe-elements';

import Alex133 from '../../images/Platform-for-Alex-xd_133.png';
import Alex135 from '../../images/Platform-for-Alex-xd_135.png';
import Alex137 from '../../images/Platform-for-Alex-xd_137.png';
import Alex139 from '../../images/Platform-for-Alex-xd_139.png';
import LivloadngImg from '../../images/lvrlbilling.gif';

const getUserDeatils = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/user/detail/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const userupdate = (data, uid) => {

    return new Promise((resolve, reject) => {
       const req = scAxios.request('/user/billing/'+uid, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            data: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};

class CheckoutForm extends Component {

  constructor(props) {
  super(props);
  this.state = {signup_success: false, forwardToken: '', Loading: false, enableShdo: false, number_of_locations: '', name_on_card: '', termchk:'', fullname: '', card_number: '', card_exp_month: '', exp_year: '', fields: {}, errors: {}, item_qnty: '', total_sum: '', item_amt: 10, };
  this.submit = this.submit.bind(this);
  }

 async submit(ev) {
 if (this.validateForm()) {
    this.props.stripe.createToken({ name: this.state.fullname }).then(({ token, error }) => {
    if (error) {

      } else {
        this.setState({ forwardToken: token.id, Loading: true, enableShdo: true, card_number: token.card.last4, card_exp_month: token.card.exp_month, exp_year: token.card.exp_year})  
        this.savedata();
      }
      });
   } else {

    this.setState({ signup_success: false, forwardToken: '', Loading: false, enableShdo: false, card_number: '', card_exp_month: '', exp_year: '', fields: {}, errors: {}, });
     toast.error('Please fill all mandatory fields.', {
                position: toast.POSITION.BOTTOM_RIGHT
          });
    }
}

handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
  }

handleCheckBoxChange = event => {
    if (event.target.checked) {
       this.setState({ termchk: true});
    } else {
      this.setState({ termchk: ''});
    }
 }  
validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;
      if (!this.state.name_on_card) {
        formIsValid = false;
        errors["name_on_card"] = "*Please enter name on card.";
      }
      if (typeof this.state.name_on_card !== "undefined") {
        if (!this.state.name_on_card.match(/^[a-zA-Z ]*$/)) {
          formIsValid = false;
          errors["name_on_card"] = "*Please enter alphabets characters only.";
        }
      }
      if (!this.state.termchk) {
        formIsValid = false;
        errors["termchk"] = "*Please check terms and condition.";
      }
      this.setState({
        errors: errors
      });
      return formIsValid;
  }

savedata = () => {
  
  var uid = this.props.forwarduId;
  var number_of_locations = this.state.number_of_locations;
  const data = {
      forwardToken: this.state.forwardToken,
      number_of_locations: this.state.number_of_locations,
      name_on_card: this.state.name_on_card,
      card_number: this.state.card_number,
      card_exp_month: this.state.card_exp_month,
      exp_year: this.state.exp_year,
  }
  userupdate(data, uid)
      .then(res => {
          if (res.status==true) {
                toast.success(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
              this.setState({ forwarduId: res.data.id,signup_success: true})  
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
            this.setState({ Loading: false });
      })
      .catch(err => {
          console.log(err);
      });
  }

  componentDidMount() {
    getUserDeatils()
      .then(res => {
          if(res.status==true){
              var userdata = res.data;
              this.setState({
                number_of_locations: userdata.number_of_locations,
                fullname: userdata.fullname,
                item_qnty: userdata.item_qnty,
                total_sum: userdata.total_sum,
              //  name_on_card: userdata.name_on_card,
              });
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
      })
      .catch(err => {
          console.log(err);
      });

  } 

  render() {

    const {Loading} = this.state;

    if (this.state.signup_success) return <Redirect to={'/userdashboard'} />

    return (
      <>
      <div className="col-md-8">
      <form className="needs-validation mb-4">
       <div className="row">
        <div className="col-md-12">
          <div className="form-group">
            <label className="input_label">CARD NUMBER</label>
            <CardNumberElement className="form-control input_custom_style" placeholder="Enter your card number" style={{base: {fontSize: '14px'}}}/> 
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6">
          <div className="form-group">
            <label className="input_label">NAME ON CARD</label>
            <input type="text" className="form-control input_custom_style" id="name_on_card" name="name_on_card" placeholder="Name on card" value={this.state.name_on_card} onChange={this.handleChange} />
            <span style={alertStyle}>{this.state.errors.name_on_card}</span>
          </div>
        </div>
        <div className="col-md-4">
          <div className="form-group">
            <label className="input_label">EXPIRATION DATE</label>
            <CardExpiryElement className="form-control input_custom_style" style={{base: {fontSize: '14px'}}}/> 
          </div>
        </div>
        <div className="col-md-2">
          <div className="form-group">
            <label className="input_label">CVV</label>
            <CardCVCElement className="form-control input_custom_style" style={{base: {fontSize: '14px'}}}/>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <label className="input_label"> ACCEPTED PAYMENT METHODS </label>
          <ul className="payment_method">
            <li><img src={Alex133} /></li>
            <li><img src={Alex135} /></li>
            <li><img src={Alex137} /></li>
            <li><img src={Alex139} /></li>
          </ul>
          <div className="custom-control custom-checkbox mb-3">

            <input type="checkbox" className="custom-control-input" id="customCheck"  name="termchk" value={this.state.termchk} onChange={this.handleCheckBoxChange} />
                    <label className="custom-control-label" htmlFor="customCheck">I've read and accept the Terms and Conditions</label>
                    <span style={alertStyle}>{this.state.errors.termchk}</span>
          </div>
        </div>
      </div>

      <div className="col-md-12">
        <div className="text-center mt-4">
          <button type="button" className="blue_btn_box" onClick={this.submit} disabled={Loading}> {Loading && <i className="fa fa-refresh fa-spin"></i>} Apply Credit Card</button>
          <p className="m-0"><a href={`/customizereview`}>Back</a></p>
        </div>
      </div> 
      </form>
      </div> 

      <div className="col-md-4">
        <div className="order_summary">
          <h4 className="black_bk_col fontweight500 font_20 mb-4">Order Summary</h4>
          <hr />
            <div className="row">
              <div className="col-md-6">
                <p className="font_14 heading_col fontweight500">Subscription:</p>
              </div>
              <div className="col-md-6">
                <p className="font_14 txt_col fontweight500 text-right">$47/mo</p>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                <p className="font_14 heading_col fontweight500">Items:</p>
              </div>
              <div className="col-md-6">
                <p className="font_14 txt_col fontweight500 text-right">$10 * {this.state.item_qnty}</p>
              </div>
            </div>
          <hr />
            <div className="row">
              <div className="col-md-6">
                <p className="font_14 heading_col fontweight500">Total</p>
              </div>
              <div className="col-md-6">
                <p className="font_16 blue_anchor_col fontweight500 text-right">${this.state.total_sum}/mo</p>
              </div>
            </div>
        </div>
      </div>

      </>
    );
  }
}

export default injectStripe(CheckoutForm);

import React, { Component } from 'react';
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';

import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import dbImg from '../../images/dashboard.png';
import profileImg from '../../images/profile_img.png';
import menuImg from '../../images/menu.png';
import shoppingLlist from '../../images/shopping-list.png';
import settingsImg from '../../images/settings.png';
import lifesaverImg from '../../images/lifesaver.png';
import dbfooterLogo from '../../images/dashboard_footer_logo.png';

const getUserDeatils = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/user/detail/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class Header extends React.Component {

  state = {
        fields: {},
        errors: {},
        forwarduId: '',
        firstname:'',
        lastname:'',
        fullname:'',
        phone:'',
        email:'',
        isprofilecomplete: '',
        signup_success:false,
        isactivationcomplete: '',
        locationCount: '',
        number_of_locations: '',
        reg_step_4: '',
    }

  handleClick() {
    toast.error("Welcome to Reviewgrowth. Please first provide us some quick information about your business", {
              position: toast.POSITION.BOTTOM_RIGHT
            });
  }
    
  componentDidMount() {

    getUserDeatils()
      .then(res => {
          if(res.status==true){
              var userdata = res.data;
              this.setState({
                firstname: userdata.firstname,
                lastname: userdata.lastname,
                fullname: userdata.fullname,
                phone: userdata.phone,
                email: userdata.email,
                isprofilecomplete: userdata.isprofilecomplete,
                isactivationcomplete: userdata.isactivationcomplete,
                number_of_locations: userdata.number_of_locations,
                reg_step_4: userdata.reg_step_4,
                locationCount: res.locationCount,
              });
              this.setState({ forwarduId: res.data.id}); 
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
      })
      .catch(err => {
          console.log(err);
      });

  }

    render() {
      let remaningLocationMsg = ''; 
      let remaningLocation = this.state.number_of_locations - this.state.locationCount;
      if(remaningLocation > 1) {
        remaningLocationMsg = remaningLocation+' more locations';
      } else {
        remaningLocationMsg = remaningLocation+' more location';
      }
      return (
        <>
          <div className="dashbord_header"> 
          <div className="dashbord_container">
            <ul className="dashbord_logo">
              <li><img src={dbImg} /></li>
              <li className="dropdown pull-right">
                <a className="dropdown-toggle" data-toggle="dropdown">
                  <img src={profileImg} /> {this.state.fullname}
                </a>
              { this.state.isprofilecomplete == 1 ?
                (<div className="dropdown-menu">
                  <a className="dropdown-item" href={`/accountsettings`}> <i className="fa fa-user-circle-o"></i> Account Settings</a>
                  <a className="dropdown-item" href={`/systemsettings`}> <i className="fa fa-gear"></i> System Settings</a>
                  <a className="dropdown-item" href={`/logout`}> <i className="fa fa-power-off"></i> Logout</a>
                </div>)
                :
                (<div className="dropdown-menu">
                  <a onClick={this.handleClick} className="dropdown-item" href="#"> <i className="fa fa-user-circle-o"></i> Account Settings</a>
                  <a onClick={this.handleClick} className="dropdown-item" href="#"> <i className="fa fa-gear"></i> System Settings</a>
                  <a className="dropdown-item" href={`/logout`}> <i className="fa fa-power-off"></i> Logout</a>
                </div>)
              }

              </li>
            </ul>
          </div>
            { this.state.isactivationcomplete != 1 ?
              (<div className="alert alert-info">
                Your account verification is not completed. Please check your registered email to verify your account.
              </div>)
              :
              <></>
            }
            { this.state.reg_step_4 != '' && this.state.number_of_locations > this.state.locationCount ?
              (<div className="alert alert-info">
                You can add {remaningLocationMsg}. Please click here to <a href="/addlocation">Add location</a>
              </div>)
              :
              <></>
            }
        </div>
        </>
        );
    }
}

export default Header;

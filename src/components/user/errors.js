import React from 'react';
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME,USER_ROLE,USER_ID,IMAGE_URL } from '../../constants';
import { scAxios } from '../..';
import userheader from './userheader.js';
import userfooter from './userfooter.js';

const getAllErrors = (data) => {
        return new Promise((resolve, reject) => {
        const req = scAxios.request('/errorlog/lists', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
             params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}
const stylecustom ={wordBreak: 'break-all',};

class errorlist extends React.Component {

	state = {
	    total: '',
	    currentPage: '',
	    LastPage:'',
	    PerPage: '',
	    FirstPageUrl:'',
	    NextPageUrl:'',
	    PrevPageUrl:'',
	    LastPageUrl:'',
	    TotalPages:'',
	    search : '',
	    loadingFirst: true,
	    errors:[],   
  	}

	refreshErrorList=(page)=>{
	    const data = {
	             page: page,
	         }
	    getAllErrors(data)
	        .then(res => {
	            if(res.status==true){
	                var errordata = res.data;
	                var records = errordata.data;
	                this.setState({ 
	                  errors: records,
	                  total: res.data.total,
	                  currentPage: errordata.current_page,
	                  PerPage: res.data.per_page,
	                  FirstPageUrl: errordata.first_page_url,
	                  NextPageUrl: errordata.next_page_url,
	                  PrevPageUrl: errordata.prev_page_url,
	                  LastPageUrl: errordata.last_page_url,
	                  LastPage: errordata.last_page,
	                  TotalPages: Math.ceil(res.data.total / res.data.per_page)
	                });
	            } else {
	               this.setState({ errors: '' }); 
	            }
	        })
	        .catch(err => {
	            console.log(err);
	        });
  	}
	componentDidMount (){   
	   this.refreshErrorList();
	} 

	render(){
	  const currentPage = this.state.currentPage;
      const previousPage = currentPage - 1;
      const NextPage = currentPage + 1;
      const LastPage = this.state.LastPage;
      const pageNumbers = [];
      for (let i = 1; i <= this.state.TotalPages; i++) {
        pageNumbers.push(i);
      }
      let srno = 0;
		return (
		    <section className="content">
		   		      <div className="row">
		   		        <div className="col-xs-12">
		   		          <div className="box">
		   		            <div className="box-body">
		   		              <div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">		   
		   		            <div className="row">
		   		              <div className="col-sm-12 table-responsive">
		   		                <table id="dataexample1" className="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
		   		                <thead>
		   		                <tr role="row">
		   		                  <th className="sorting_asc col-sm-2" tabindex="0" aria-controls="dataexample1" rowspan="1" colSpan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style={{ width: '70px' }}>Sr.No.</th>
		   		                  <th className="sorting col-sm-2" tabindex="0" aria-controls="dataexample1" rowspan="1" colSpan="1" aria-label="Browser: activate to sort column ascending" style={{ width: '190px' }}>Error Message</th>
		   		                  <th className="sorting col-sm-2" tabindex="0" aria-controls="dataexample1" rowspan="1" colSpan="1" aria-label="Platform(s): activate to sort column ascending" style={{ width: '190px' }}>Line Number</th>
		   		                  <th className="sorting col-sm-2" tabindex="0" aria-controls="dataexample1" rowspan="1" colSpan="1" aria-label="Platform(s): activate to sort column ascending" style={{ width: '190px' }}>File Name</th>
		   		                  <th className="sorting col-sm-2" tabindex="0" aria-controls="dataexample1" rowspan="1" colSpan="1" aria-label="Platform(s): activate to sort column ascending" style={{ width: '190px' }}>IP Address</th>
		   		                  <th className="sorting col-sm-2" tabindex="0" aria-controls="dataexample1" rowspan="1" colSpan="1" aria-label="Platform(s): activate to sort column ascending" style={{ width: '190px' }}>Log In Id</th>
		   		                  <th className="sorting col-sm-2" tabindex="0" aria-controls="dataexample1" rowspan="1" colSpan="1" aria-label="Engine version: activate to sort column ascending" style={{ width: '255px' }}>Browser</th>
		   		                </tr>
		   		                </thead>
		   		                <tbody>
		   		                {
		   		                        this.state.errors.length > 0
		   		                    ?
		   		                        this.state.errors.map(error => {
		   		                  return(<tr role="row" className="odd" key={error.id}>
		   		                     <td className="sorting_1 col-sm-2">{srno=srno+1}</td>
		   		                    <td style={ stylecustom }>{error.error_message}</td>
		   		                    <td>{error.line_number}</td>
		   		                    <td style={ stylecustom }>{error.file_name}</td>
		   		                    <td>${error.ip_address}</td>
		   		                    <td>{error.loggedin_id}</td>
		   		                    <td>{error.browser}</td>
		   		                  </tr>);
		   		                  })
		   		                    :
		   		                    <tr className="no_records">
		   		                      <td><h2>No Records founds!</h2></td>
		   		                    </tr>
		   		                  }
		   		                </tbody>
		   		              </table>
		   		            </div>
		   		          </div>
		   		            <div className="row">
		   		                { pageNumbers.length > 1 ?
		   
		   		                  <div className="paggination-text">
		   		                    <ul className="pagination pagination-sm">
		   		                      {this.state.PrevPageUrl != null ? 
		   		                    <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshErrorList.bind(this, previousPage)}>Previous</a></li>
		   		                    : ''
		   		                  }
		   		                        {pageNumbers.map(page => {
		   		                        return (<li className={currentPage == page ? 'activelink page-item' : 'page-item' } key={page}>
		   		                          <a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshErrorList.bind(this, page)}>{page}</a>
		   		                          </li>);
		   		                      })}
		   
		   		                    {this.state.NextPageUrl != null ? 
		   		                    <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshErrorList.bind(this, NextPage)}>Next</a></li>
		   		                    : ''
		   		                  }
		   		                      {this.state.LastPage != null && currentPage!=LastPage? 
		   		                    <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshErrorList.bind(this, LastPage)} >Last</a></li>
		   		                    : ''
		   		                  }
		   		                    
		   		                    </ul>
		   		                  </div> : ''
		   		                  }
		   		   
		   		            </div>
		   		          </div>
		   		        </div>
		   		      
		   		          </div>
		   		          
		   		        </div>
		   		        
		   		      </div>
		   		      
		   		    </section>


		   		    
		    
  		);
	
	}  
}

export default errorlist;
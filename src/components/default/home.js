import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { API_TOKEN_NAME, LOGIN_PAGE_PATH, IS_ACTIVE, } from '../../constants';
import { validateUserToken } from '../PrivateRoute';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';

import logoWhite from '../../images/30days_logo.png';
import registerLogo from '../../images/register_logo.png';

const alertStyle = {
  color: 'red',
};

class home extends React.Component {

  state = {
        fields: {},
        errors: {},
  };

    render(){
      
      return (

      <div>
      
      <Route component={Header} />

      <div className="container ">
        <div className="row starbuck_row">
          <div className="col-md-6">
            <h1 className="fontweight500 heading_col font_32">Try ReviewGrowth for 30 days</h1>
            <p className="font_16 heading_col fontweight500 width_435">Create an account to try ReviewGrowth today. If you’re not completely satisfied with our review platform, cancel anytime no questions asked.</p>
            <ul className="instruction width_435">
              <li> For small to large businesses that understand the value of a good reputation online</li>
              <li>Naturally and effectively grow your online reviews for your business</li>
              <li>Whether you are 4.9 stars or 1.5 stars, ReviewGrowth is a must have tool to stay competitive in the marketplace.</li>
            </ul>
          <a href="#" className="blue_small_btn" data-toggle="modal" data-target="#myModal"><i className="fa fa-youtube-play youtube_icon pr-2" aria-hidden="true"></i>Click to Watch Signup Video</a>
          </div>  
          <div className="col-md-6">
            <img src={registerLogo} className="mb-4" />
            <h6 className="heading_col font_16">Landing Page</h6>
            <p className="txt_col fontweight500 font_12 mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
    
            <p className="txt_col fontweight500 font_12"> <a href="/signup" className="blue_anchor_col">Sign Up</a> <a href="/login" className="blue_anchor_col ml-5">Sign In</a></p>

          </div>
        </div>
      </div>

      <Route component={Footer} />
    
    <ToastContainer autoClose={5000} /> 

  </div>

      );
    }

}

export default home;
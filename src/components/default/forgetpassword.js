import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { API_TOKEN_NAME, LOGIN_PAGE_PATH, IS_ACTIVE, } from '../../constants';
import { validateUserToken } from '../PrivateRoute';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';

import logoWhite from '../../images/30days_logo.png';
import registerLogo from '../../images/register_logo.png';

const forgetpasswordUser = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/forgetpassword', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            },
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

class forgetpassword extends React.Component {

  state = {
        email: '',
        enableLoginBtn: false,
        user_active: false,
        fields: {},
        errors: {},
  };

handleChange = event => {
        this.setState({ errors:''});
        let fields = this.state.fields;
        fields[event.target.name] = event.target.value;
        this.setState({
            fields
        }, () => this.validateForm());
}

handleSubmit = event => {
  event.preventDefault();
      if (this.validateForm()) {
          let fields = {};
          fields["email"] = "";
          this.setState({ fields: fields });

          const data = {
              email: this.state.fields.email
          }

          forgetpasswordUser(data)
              .then(res => {
                  if (res.status==true) {
                    toast.success(res.message, {
                      position: toast.POSITION.BOTTOM_RIGHT
                    });
                  }
                  else {
                      toast.error(res.message, {
                          position: toast.POSITION.BOTTOM_RIGHT
                      });
                  }
              })
              .catch(err => {
                  toast.error('Error occured', {
                      position: toast.POSITION.BOTTOM_RIGHT
                  });
              });
      }
      else {
          toast.error('Please provide Email', {
              position: toast.POSITION.BOTTOM_RIGHT
          });
      }
} 

validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!fields["email"]) {
          formIsValid = false;
          errors["email"] = "*Please enter your email.";
      }

      if (typeof fields["email"] !== "undefined") {
          let lastAtPos = fields["email"].lastIndexOf('@');
          let lastDotPos = fields["email"].lastIndexOf('.');
          if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
              formIsValid = false;
              errors["email"] = "Email is not valid";
            }
      }

      this.setState({
          errors: errors
      });
      return formIsValid;
  }
    render(){
      return (

      <div>
      
      <Route component={Header} />

      <div className="container ">
        <div className="row starbuck_row">
          <div className="col-md-6">
            <h1 className="fontweight500 heading_col font_32">Try ReviewGrowth for 30 days</h1>
            <p className="font_16 heading_col fontweight500 width_435">Create an account to try ReviewGrowth today. If you’re not completely satisfied with our review platform, cancel anytime no questions asked.</p>
            <ul className="instruction width_435">
              <li> For small to large businesses that understand the value of a good reputation online</li>
              <li>Naturally and effectively grow your online reviews for your business</li>
              <li>Whether you are 4.9 stars or 1.5 stars, ReviewGrowth is a must have tool to stay competitive in the marketplace.</li>
            </ul>
          <a href="#" className="blue_small_btn" data-toggle="modal" data-target="#myModal"><i className="fa fa-youtube-play youtube_icon pr-2" aria-hidden="true"></i>Click to Watch Signup Video</a>
          </div>  
          <div className="col-md-6">
            <img src={registerLogo} className="mb-4" />
            <h6 className="heading_col font_16">Forgot Password</h6>
            <p className="txt_col fontweight500 font_12 mb-4">Recover your account password here</p>
    
            <form onSubmit={this.handleSubmit} id="forgotpwdform" className="needs-validation" novalidate>

              <div className="row">
                <div className="col-sm-12">
                <div className="form-group">
               
                <input type="text" class="form-control input_custom_style" name="email" id="email" value={this.state.fields.email} onChange={this.handleChange} placeholder="Email" required="" autocomplete="off"/>
                <span style={alertStyle}>{this.state.errors.email}</span> 
                  </div>
                </div>
              </div>
              
              <button type="submit" className="blue_btn mt-3"> Reset </button>
            </form>

            <p className="txt_col fontweight500 font_12 mt-3"> <a href="/login" className="blue_anchor_col">Already have an account? Sign In</a> <a href="/signup" className="blue_anchor_col ml-5">Don't have an account? Sign Up</a></p>

          </div>
        </div>
      </div>

      <Route component={Footer} />
    
    <ToastContainer autoClose={5000} /> 

  </div>

      );
    }

}

export default forgetpassword;